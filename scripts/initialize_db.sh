#/bin/bash
# This is to initialize a new database for tests

EXECUTE=true
if [ -z "$DBUSER" ]; then
	#default to not verbose if not set.
	echo "DB USER NOT SPECIFIED SETTING TO root" >&2
	DBUSER="root"
fi


while getopts ":?ns" opt; do
	case $opt in
		n)
			EXECUTE=false
			;;
		\?)
			echo "example:"
			echo "  > initialize_db.sh"
			echo "options:"
			echo "  -n test mode. only displays potential changes"
			exit 1
			;;
	esac
done

shift $((OPTIND-1))

db='test'
prefix='TST'
passwd=$db

# Created the regular and test databases
echo "CREATE DATABASE IF NOT EXISTS $db;
CREATE USER IF NOT EXISTS '$db'@'%' IDENTIFIED BY '$passwd';
GRANT ALL PRIVILEGES ON $db.* TO '$db'@'%';
FLUSH PRIVILEGES;" > /tmp/$db.sql

database_url="${prefix}_DATABASE_URL"

# echo the ENV commands, do these before the exec so they can be added to the bash profile file even if
# the databases already exist.
echo "export ${database_url}='${db}:${passwd}@tcp(localhost:3306)/${db}?parseTime=true&charset=utf8mb4'"

if $EXECUTE; then
	mysql -u ${DBUSER} < /tmp/$db.sql || exit 1;
else
	cat /tmp/$db.sql
fi
